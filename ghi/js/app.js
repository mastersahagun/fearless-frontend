function createCard(name, description, pictureUrl, starts, ends, location) {
    return `
        <div class="card shadow p-3 mb-5 bg-body-tertiary rounded">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
            <h5 class="card-title">${name}</h5>
            <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
            <p class="card-text">${description}</p>
        </div>
        <div class="card-footer text-muted">
        ${starts} - ${ends}
        </div>
        </div>
    `;
}

function createError(){
    return `
        <div class="alert alert-danger">
        Bad request!
        </div>
    `
}

window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
        const response = await fetch(url);

        if (!response.ok) {
            const html2 = createError()
            const main = document.querySelector('main')
            main.innerHTML += html2
        } else {
            const data = await response.json();

            let counter = 0

            for (let conference of data.conferences) {
                counter += 1
                const detailUrl = `http://localhost:8000${conference.href}`;
                const detailResponse = await fetch(detailUrl);

                if (detailResponse.ok) {
                    const details = await detailResponse.json();
                    const title = details.conference.name;
                    const description = details.conference.description;

                    const sDate = new Date(details.conference.starts)
                    const starts = sDate.toLocaleDateString()
                    const eDate = new Date(details.conference.ends)
                    const ends = eDate.toLocaleDateString()

                    const location = details.conference.location.name

                    const pictureUrl = details.conference.location.picture_url;
                    const html = createCard(title, description, pictureUrl, starts, ends, location);
                    const column1 = document.querySelector('.col1');
                    const column2 = document.querySelector('.col2');
                    const column3 = document.querySelector('.col3');
                    if(counter %3===1){
                    column1.innerHTML += html;
                } else if(counter%3===2){
                    column2.innerHTML += html;
                } else{
                    column3.innerHTML += html;
                }
            }
            }
        }
    } catch (e) {
        console.error(e);
    }
});
